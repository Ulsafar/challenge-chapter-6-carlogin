
const userService = require("../services/userService");

async function register(req, res) {
    try {
        const user = await userService.create(req);
        const { data } = user;

        if(!data){
            res.status(500).json({
                success: false,
                error: true,
                data,
            });
        }

        res.status(200).json({
            success: true,
            error: false,
            message: "User successfully created!",
            data
        });
    } catch (err) {
        res.status(500).json({
            success: false,
            error: true,
            data: null,
            message: "Can't create account"
        });
        console.log(err)
    };
}

async function login(req, res) {
    try {
        const result = await userService.checkUser(req);

        const { data, token, message } = result;
        if (!data) {
            res.status(401).json({
                success: false,
                error: true,
                data: null,
                message
            })
            return;
        }

        const userInfo = {
            id: data.id,
            email: data.email,
            token
        }

        res.status(200).json({
            success: true,
            data: userInfo,
            message
        })
    } catch (error) {
        res.status(400).json({
            success: false,
            error: true,
            data: null,
            message: error
        })
        console.log(error);
    }
}

module.exports = { register, login}