const { User } = require("../models");

module.exports = {
    create(requestBody) {
        return User.create(requestBody);
    },
    findOne(condition) {
        return User.findOne(condition);
    },

    findByPk(id) {
        return User.findByPk(id);
    }
}