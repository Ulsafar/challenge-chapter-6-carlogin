const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

function encryptPassword(password){
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, 10, (err, encryptPassword)=>{
            if(!!err){
                reject(err);
                return;
            }
            console.log(encryptPassword);
            console.log(password);
            resolve(encryptPassword)
        })
    })
}

async function decryptPass(encryptedPassword, password) {
    return new Promise((resolve, reject) => {
      bcrypt.compare(password, encryptedPassword, (err, isPasswordCorrect) => {
        if (err) {
          reject(err);
        } else {
          resolve(isPasswordCorrect);
        }
      });
    });
  }

function createToken(payload) {
    return jwt.sign(payload, "secret");
}

module.exports = {encryptPassword,decryptPass,createToken}