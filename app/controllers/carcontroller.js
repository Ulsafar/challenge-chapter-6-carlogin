const carService = require("../services/carService");

async function findAll(req, res) {
    try {
        const data = await carService.list()
        return res.status(200).json({
            success: true,
            error: false,
            message: " Data successfully populated",
            data: data
        });
    } catch (error) {
        return res.status(400).json({
            success: false,
            error: true,
            data: null,
            message: error
        });
    }
}

async function create(req, res) {
        try {
            const carcreate = await carService.create(req);
            const { data } = carcreate;

        if(!data){
            res.status(500).json({
                success: false,
                error: true,
                data,
            });
        }
        res.status(200).json({
            success: true,
            error: false,
            message: "User successfully created!",
            data
        });
    } catch (err) {
        res.status(500).json({
            success: false,
            error: true,
            data: null,
            message: "Can't create account"
        });
        console.log(err)
    };
    }


// async function destroy(req, res) {
//     try {
//         const data = await carService.delete(req.params.id)
//         carService.update(req.params.id, { deletedBy: req.user.id }, false)
//         console.log('user', req.user.id);
//         return res.status(200).json({
//             success: true,
//             error: false,
//             data: data,
//             message: " Data successfully deleted"
//         })
//     } catch (err) {
//         console.log(err);
//         return res.status(400).json({
//             success: false,
//             error: true,
//             data: null,
//             message: err
//         });
//     }
// }

// async function update(req, res,) {
//     let upload = multer({ storage: storage, fileFilter: imageFilter }).single('image');
//     upload(req, res, async (err) => {
//         await carService.findOne(req.params.id).then(car => {
//             carService.update(
//                 req.params.id,
//                 {
//                     name: req.body.name,
//                     price: req.body.price,
//                     size: req.body.size,
//                     image: req.file ? req.file.originalname : car.image,
//                     updatedBy: req.user.id
//                 }, false
//             );
            
//         });

//         res.json({
//             data: req.post,
//             message: "Success Update"
//         })
//     });
// }

module.exports = { create, findAll};