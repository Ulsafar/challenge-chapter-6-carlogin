const userController = require("./user");
const auth = require("./auth");
const carController = require("./carcontroller")

module.exports = {
    userController,
    auth,
    carController
}